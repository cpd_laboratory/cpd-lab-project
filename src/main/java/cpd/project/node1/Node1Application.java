package cpd.project.node1;

import cpd.project.node1.communication.CommunicationManager;
import cpd.project.node1.pubsub.PubSubManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.io.IOException;

@SpringBootApplication
public class Node1Application {

	public static void main(String[] args) throws IOException {
		SpringApplication springApplication = new SpringApplication(Node1Application.class);
		ApplicationContext applicationContext = springApplication.run(args);
		CommunicationManager manager = applicationContext.getBean(CommunicationManager.class);
		PubSubManager.configureSubs();
		try {
			manager.startConnection();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
