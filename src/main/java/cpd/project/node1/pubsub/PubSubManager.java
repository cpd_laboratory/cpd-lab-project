package cpd.project.node1.pubsub;

import com.google.cloud.spring.pubsub.core.PubSubTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PubSubManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(PubSubManager.class);
    private static PubSubTemplate pubSubTemplate;
    private static String lastMessageSent = "";

    public PubSubManager(PubSubTemplate pubSubTemplate) {
        PubSubManager.pubSubTemplate = pubSubTemplate;
    }
    public static void configureSubs() {
        List<String> subscriptions = GcpConfig.getSubscriptions();
        for(String subscription : subscriptions) {
            pubSubTemplate.subscribe(subscription, (message) -> {
                String actualMessage = message.getPubsubMessage().getData().toStringUtf8();
                if(!actualMessage.equals(lastMessageSent)) {
                    LOGGER.info("Message on sub " + subscription + ": " + actualMessage);
                }
                message.ack();
            });
        }
    }
    public static void publish(int topic, String text) {
        List<String> topics = GcpConfig.getTopicsToPublish();
        String topicString = "topic" + topic;
        String topicToPublish = null;
        for(String whichTopic:topics) {
            if(whichTopic.equals(topicString)) {
                topicToPublish = whichTopic;
                break;
            }
        }
        if(topicToPublish == null) {
            System.out.println("You don't have permission to write on this topic");
        } else {
            sendOnTopic(topicToPublish, text);
            lastMessageSent = text;
        }
    }
    private static void sendOnTopic(String whichTopic, String text) {
        pubSubTemplate.publish(whichTopic, text);
    }

}
