package cpd.project.node1.pubsub;


import com.google.cloud.spring.pubsub.core.PubSubTemplate;
import com.google.cloud.spring.pubsub.integration.AckMode;
import com.google.cloud.spring.pubsub.integration.inbound.PubSubInboundChannelAdapter;
import com.google.cloud.spring.pubsub.integration.outbound.PubSubMessageHandler;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

import java.util.Arrays;
import java.util.List;

/**
* This node publishes on topics 2;3 and is subscribed to topics 1;2
*/

@Configuration
public class GcpConfig {
    private static List<String> topicsToPublish = Arrays.asList("topic2", "topic3");
    private static List<String> subscriptions = Arrays.asList("topic1-sub", "topic2-sub");

    public static List<String> getTopicsToPublish() {
        return topicsToPublish;
    }

    public static List<String> getSubscriptions() {
        return subscriptions;
    }
}
