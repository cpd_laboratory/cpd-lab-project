package cpd.project.node1.communication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * This class acts as Client for the next node and as Server for the previous node in the ring
 */
@Component
public class CommunicationManager implements LocalConfig {
    private static final String TOKEN_STRING = "token";
    public static boolean hasToken = false;
    public static boolean leaderElected = false;
    public static int sentId = 0;
    private ServerConfig serverConfig = new ServerConfig(PREV_PORT);
    private ClientConfig clientConfig = new ClientConfig();

    public CommunicationManager() throws IOException {
    }

    public void startConnection() throws IOException {
        System.out.println("Server started successfully!");
        clientConfig.startConnection(LOCALHOST_IP, NEXT_PORT);
        serverConfig.acceptClient();
        String currentTokenString = String.valueOf(nodeId);
        while(true) {
            if(currentTokenString.equals(TOKEN_STRING)) {
                hasToken = true;
                long receiveTime = System.currentTimeMillis();
                System.out.println("Token received at " + new Date(receiveTime) + ". You will keep the token for 10 seconds");
                ExecutorService executorService = Executors.newSingleThreadExecutor();
                executorService.submit(new TokenProcess());
                executorService.shutdown();
                try {
                    executorService.awaitTermination(20, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if(!executorService.isTerminated()) {
                        System.out.println("10 seconds have passed. You are giving away the token");
                    }
                    executorService.shutdownNow();
                    hasToken = false;
                    System.out.println("Token given away");
                }
                clientConfig.getOutputWriter().println(currentTokenString);
            } else {
                if (!leaderElected) {
                    if (sentId < nodesCount) {
                        int value = Integer.parseInt(currentTokenString);
                        int minID = -1;
                        if (value < nodeId) {
                            minID = value;
                        } else {
                            minID = nodeId;
                        }
                        clientConfig.getOutputWriter().println(String.valueOf(minID));
                        System.out.println("Sent '" + String.valueOf(minID) + "'");
                        sentId++;
                    } else {
                        int value = Integer.parseInt(currentTokenString);
                        if (value == nodeId) {
                            System.out.println("Leader selected, ID = " + value);
                            leaderElected = true;
                            clientConfig.getOutputWriter().println(TOKEN_STRING);
                            System.out.println("Sent '" + TOKEN_STRING + "'");
                        } else {
                            clientConfig.getOutputWriter().println(String.valueOf(value));
                            System.out.println("Sent '" + String.valueOf(value) + "'");
                        }
                    }
                }
            }
            currentTokenString = serverConfig.getInputBufferedReader().readLine();
            System.out.println("Read " + currentTokenString);
        }
    }

    public void stopConnection() throws IOException {
        serverConfig.stopConnection();
        clientConfig.stopConnection();
    }

}
