package cpd.project.node1.communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerConfig {
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private BufferedReader in;

    /**
     * Start the server on specified port
     * @param port the port to run server on
     */
    public ServerConfig(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    public void acceptClient() throws IOException {
        clientSocket = serverSocket.accept();
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }

    public void stopConnection() throws IOException {
        in.close();
        clientSocket.close();
        serverSocket.close();
    }

    public BufferedReader getInputBufferedReader() {
        return in;
    }
}
