package cpd.project.node1.communication;

public interface LocalConfig {
    final String LOCALHOST_IP = "127.0.0.1";
    final int PREV_PORT = 8030;
    final int NEXT_PORT = 8010;
    final int nodeId = 1;
    final int nodesCount = 3;
}
