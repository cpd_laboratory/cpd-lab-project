package cpd.project.node1.communication;

import cpd.project.node1.pubsub.PubSubManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TokenProcess implements Runnable {
    @Override
    public void run() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while(true) {
            try {
                String userInput = reader.readLine();
                String[] userTokens = userInput.split("-");
                if(userTokens.length == 2) {
                    int whichTopic = Integer.parseInt(userTokens[0]);
                    String message = userTokens[1];
                    if(CommunicationManager.hasToken) {
                        PubSubManager.publish(whichTopic, message);
                        System.out.println("Message sent on topic " + whichTopic);
                    } else {
                        System.out.println("Sorry, you don't have the token");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
