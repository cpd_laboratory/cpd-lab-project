package cpd.project.node1.communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientConfig {
    private Socket clientSocket;
    private PrintWriter out;

    /**
     * Function to start connection
     * @param ip IP
     * @param port PORT
     * @throws IOException
     */
    public void startConnection(String ip, int port) throws IOException {
        boolean connected = false;
        while (!connected) {
            try {
                clientSocket = new Socket(ip, port);
                if (clientSocket != null) {
                    connected = true;
                    System.out.println("Connection to server successful");
                    out = new PrintWriter(clientSocket.getOutputStream(), true);
                }
            } catch (IOException e) {
                continue;
            }
        }
    }

    /**
     * Closes the connection
     * @throws IOException
     */
    public void stopConnection() throws IOException {
        out.close();
        clientSocket.close();
    }

    public PrintWriter getOutputWriter() {
        return out;
    }
}
